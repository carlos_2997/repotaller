var clientesObtenidos;

function getCustomers() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      //console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarClientes() {
  var JSONProductos = JSON.parse(clientesObtenidos);
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var thead = document.createElement("thead");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  var nuevaFilaHead = document.createElement("tr");

  var columnaContactName = document.createElement("th");
  columnaContactName.innerText = "Contact Name";

  var columnaCiudad = document.createElement("th");
  columnaCiudad.innerText = "City";

  var columnaBandera = document.createElement("th");
  columnaBandera.innerText = "Flag";

  nuevaFilaHead.appendChild(columnaContactName);
  nuevaFilaHead.appendChild(columnaCiudad);
  nuevaFilaHead.appendChild(columnaBandera);

  thead.appendChild(nuevaFilaHead);
  tabla.appendChild(thead);


  for(var i = 0 ; i < JSONProductos.value.length ; i++){
    //console.log(JSONProductos.value[i]);
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONProductos.value[i].City;

    var columnaImg = document.createElement("td");
    var imagen = document.createElement("img");
    imagen.classList.add("flag");
    if(JSONProductos.value[i].Country == "UK"){
      imagen.src = rutaBandera+"United-Kingdom.png";
    }else{
      imagen.src = rutaBandera+JSONProductos.value[i].Country+".png";
    }
    columnaImg.appendChild(imagen);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaImg);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
