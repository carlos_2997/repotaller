var productosObtenidos;

function getProductos() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var params = "$filter=UnitPrice gt 15&$orderby=UnitPrice desc&$count=true&$select=ProductID,ProductName,UnitPrice,UnitsInStock";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      //console.log(request.responseText);
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  }

  request.open("GET", url+"?"+params, true);
  request.send();
}

function procesarProductos() {
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("divTablaProductos");
  var tabla = document.createElement("table");
  var thead = document.createElement("thead");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  var nuevaFilaHead = document.createElement("tr");

  var columnaProductName = document.createElement("th");
  columnaProductName.innerText = "Product Name";

  var columnaUnitPrice = document.createElement("th");
  columnaUnitPrice.innerText = "Unit Price";

  var columnaUnitInStock = document.createElement("th");
  columnaUnitInStock.innerText = "Unit In Stock";

  nuevaFilaHead.appendChild(columnaProductName);
  nuevaFilaHead.appendChild(columnaUnitPrice);
  nuevaFilaHead.appendChild(columnaUnitInStock);

  thead.appendChild(nuevaFilaHead);
  tabla.appendChild(thead);


  for(var i = 0 ; i < JSONProductos.value.length ; i++){
    console.log(JSONProductos.value[i]);
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
